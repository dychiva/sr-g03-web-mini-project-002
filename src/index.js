import React , { Suspense }  from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import "bootstrap/dist/css/bootstrap.min.css";
import {createStore, applyMiddleware} from 'redux';
import {rootReducer} from '../src/redux/reducers/rootReducers';
import thunk from 'redux-thunk';
import logger from 'redux-logger/src'
// import {BrowserRouter} from 'react-router-dom';
import {Provider} from 'react-redux';

const middlewares = [thunk, logger]
const store = createStore(rootReducer, applyMiddleware(...middlewares));

ReactDOM.render (
    <Provider store={store}>
        {/* <BrowserRouter> */}
        <Suspense fallback={(<div>Loading....</div>)}>
            <React.StrictMode>
                <App/>
            </React.StrictMode>
        </Suspense>
        {/* </BrowserRouter> */}
    </Provider>,
    document.getElementById('root')
);

serviceWorker.unregister();
