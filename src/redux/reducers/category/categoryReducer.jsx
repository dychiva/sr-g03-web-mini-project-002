import {
  FETCH_CATEGORIES,
  DELETE_CATEGORY,
  ADD_CATEGORY,
  EDIT_CATEGORY,
} from "../../actions/category/actionType";
const initState = {
  category: [],
};
export const categoryReducer = (state = initState, action) => {
  switch (action.type) {
    case FETCH_CATEGORIES:
      return {
        ...state,
        category: action.category,
      };
    case ADD_CATEGORY:
      return {
        ...state,
        category: action.category,
      };
    case DELETE_CATEGORY:
      const id = action.id;
      var newCategory = state.category.filter((cat) => cat._id !== id);
      return {
        ...state,
        category: newCategory,
      };
    case EDIT_CATEGORY:
      return {
        ...state,
        category: action.category,
      };
    default:
      return state;
  }
};
