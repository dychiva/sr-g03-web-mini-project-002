import { articleReducer } from "./article/articleReducer";
import { combineReducers } from "redux";
import { categoryReducer } from "./category/categoryReducer";
const reducer = {
  articleReducer: articleReducer,
  categoryReducer: categoryReducer,
};

export const rootReducer = combineReducers(reducer);
