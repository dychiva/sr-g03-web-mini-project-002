import Axios from "axios";
import {
  FETCH_CATEGORIES,
  DELETE_CATEGORY,
  ADD_CATEGORY,
  EDIT_CATEGORY,
} from "./actionType";

export const fetchCategories = () => {
  const innerFetchCategory = async (dispatch) => {
    const result = await Axios.get("http://110.74.194.125:3535/api/category");
    dispatch({
      type: FETCH_CATEGORIES,
      category: result.data.data,
    });
  };
  return innerFetchCategory;
};

export const addCategory = (category) => {
  const innerAddCategory = async (dispatch) => {
    await Axios.post("http://110.74.194.125:3535/api/category", category).then(
      (res) => {
        alert(res.data.message);
      }
    );

    //Get data after delete
    const result = await Axios.get("http://110.74.194.125:3535/api/category");
    dispatch({
      type: ADD_CATEGORY,
      category: result.data.data,
    });
  };
  return innerAddCategory;
};

export const deleteCategory = (id) => {
  const innerDeletetArticle = async (dispatch) => {
    const result = await Axios.delete(
      `http://110.74.194.125:3535/api/category/${id}`
    );
    dispatch({
      type: DELETE_CATEGORY,
      id: id,
    });
  };
  return innerDeletetArticle;
};

export const editCategory = (id, category) => {
  const innerEditCategory = async (dispatch) => {
    await Axios.put(
      `http://110.74.194.125:3535/api/category/${id}`,
      category
    ).then((res) => {
      alert(res.data.message);
    });

    //Get data after delete
    const result = await Axios.get("http://110.74.194.125:3535/api/category");
    dispatch({
      type: EDIT_CATEGORY,
      category: result.data.data,
    });
  };
  return innerEditCategory;
};
