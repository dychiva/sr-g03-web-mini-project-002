import thunk from 'redux-thunk';
import logger from 'redux-logger'
import { rootReducer } from '../reducers/rootReducers';
import {createStore, applyMiddleware} from 'redux';
const middleWare = [thunk, logger]
export  const store = createStore(rootReducer, applyMiddleware(...middleWare));

