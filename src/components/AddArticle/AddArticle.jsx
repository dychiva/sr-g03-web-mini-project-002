import React, { Component } from "react";
import { Form, Button, Row, Image, Col } from "react-bootstrap";
import { addArticle } from "../../redux/actions/article/articleAction";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

class AddArticle extends Component {
  state = {
    imageFile:
      "https://www.grwa.com.au/wp-content/themes/ap-gateway/images/default_avatar.jpg",
  };

  constructor(props) {
    super(props);
    this.state = {
      article: {
        title: "",
        description: "",
      },
      isSubmit: false,
    };
  }

  customFileInput = (event) => {
    const reader = new FileReader();
    reader.onload = () => {
      if (reader.readyState === 2) {
        this.setState({ imageFile: reader.result });
      }
    };
    reader.readAsDataURL(event.target.files[0]);
  };

  handleChange = (e) => {
    this.setState({
      article: {
        ...this.state.article,
        title: e.target.value,
        description: e.target.value,
      },
    });
    console.log(this.state.article.description);
  };

  handleOnSubmit = () => {
    this.props.addArticle(this.state.article);
  };


  handleTypeCategory = (e) => {
    this.setState({
        cate_id: e.target.value
    });
};

  //For validate

  render() {
    const { imageFile } = this.state;
    return (
      <div className="container">
        <Row>
          <Col>
            <h3 className="mt-4">Add Article</h3>
            <div className="row-left">
              <Form>
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Title</Form.Label>
                  <Form.Control
                    id="title"
                    name="title"
                    type="text"
                    value={this.props.article.title}
                    placeholder="Input title"
                    aria-describedby="inputGroupPrepend"
                    required
                    onChange={ this.handleChange}
                  />
                </Form.Group>
                <fieldset>
                  <Form.Group as={Row}>
                    <Form.Label as="legend" column sm={2}>
                      Category :
                    </Form.Label>

                    {
                        this.props.category.map((type,index)=>{
                          return(
                            <Form.Check
                            key={index}
                            style={{ float: "left", marginLeft: "10px" }}
                            type="radio"
                            label={type.name}
                            className="mt-2"
                            name="category"
                            id="category"
                            value={type._id}
                            onChange={this.handleChange}
                          />
                          );
                        })
                    }

                  </Form.Group>
                </fieldset>
                <Form.Group controlId="formBasicPassword">
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                    name="description"
                    id="description"
                    type="text"
                    placeholder="Input Descrition"
                    aria-describedby="inputGroupPrepend"
                    required
                    onChange={this.handleChange}
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Thumbnail</Form.Label>
                  <Form.File
                    id="custom-file"
                    label="Custom file input"
                    custom
                    onChange={this.customFileInput}
                  />
                </Form.Group>
                <Button
                  variant="info"
                  type="submit"
                >
                  SUBMIT
                </Button>
              </Form>
            </div>
          </Col>
          <Col xs={6} md={4}>
            <div className="row-right">
              <Image
                style={{ width: 250, marginTop: 70 }}
                src={imageFile}
                alt="Default"
              />
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    article: state.articleReducer.article,
    category: state.categoryReducer.category,
  };
};

const mapDispatchToProps = (dispatch) => {
  const boundActionCreators = bindActionCreators(
    {
      addArticle,
    },
    dispatch
  );
  return boundActionCreators;
};

export default connect(mapStateToProps, mapDispatchToProps)(AddArticle);
