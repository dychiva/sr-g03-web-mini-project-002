// import React from 'react';
// import {Row,Col,Card} from 'react-bootstrap';
// const ViewArticle = (props) => (
//   <div className="contain-view">
//     <Row >
//         <Col lg={4} md={4}>
//             <Card>
//                 <Card.Img variant="top" src='https://www.planetware.com/wpimages/2020/02/france-in-pictures-beautiful-places-to-photograph-eiffel-tower.jpg' />
//             </Card>
//         </Col>
//         <Col lg={8} md={8}>
//             <div>
//                 <h3>How to Stay in the Moment: Take a Picture | WIRED</h3>
//                 <p>Create Date:<span> 2020-03-22</span></p>
//                 <p>Category-SPORN</p>
//                 <div>
//                     <p><b>The Eiffel Tower is so monumental that it's difficult to capture by camera. One solution is to photograph from a distance. Across the Seine River</b></p>
//                 </div>
//             </div>
//         </Col>
//     </Row>
//   </div>
// );
// export default ViewArticle;





import {Row,Col,Card} from 'react-bootstrap';
import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {viewArticle} from '../../redux/actions/article/articleAction';
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'

class ArticleList extends Component {
  constructor(props){
    super(props);
    this.state={
    }
  }
  componentWillMount() {
        var id = this.props.match.params.id;
        this.props.viewArticle(id);
  }

    //Convert Date
    convertDate = (created_date) =>{
      let parameterDate = created_date;
      let year = parameterDate.substring(0,4);
      let month = parameterDate.substring(5,7);
      let day = parameterDate.substring(8,10);
      let formalDate = year+ "-" + month + "-" + day;
      return formalDate;
  }

  render() {
      
      
    return (
  <div className="contain-view">
    <Row >
        <Col lg={4} md={4}>
            <Card>
                <Card.Img variant="top" src={this.props.articleDetail.image} width="100%" alt={this.props.articleDetail.image} />
            </Card>
        </Col>
        <Col lg={8} md={8}>
            <div>
                <h3>{this.props.articleDetail.title}</h3>
                <p>Create Date:<span>2012-12-06</span></p>
                <p>Category-SPORN</p>
                <div>
                    <p>{this.props.articleDetail.description}</p>
                </div>
            </div>
        </Col>
    </Row>
  </div>
    );
  }
}


const mapStateToProps = (state) =>{
  return{
    articleDetail : state.articleReducer.articleDetail,
  }
}

const mapDispatchToProps = (dispatch)=>{
  const boundActionCreators = bindActionCreators({
    viewArticle
  },dispatch)
  return boundActionCreators
}

export default connect(mapStateToProps, mapDispatchToProps)(ArticleList)