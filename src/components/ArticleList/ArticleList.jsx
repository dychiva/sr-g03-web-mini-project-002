import React, { Component } from "react";
import { Container, Row, Col,Form, Button,span } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import {fetchArticles, deleteArticle, viewArticle} from '../../redux/actions/article/articleAction';
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import {Link} from 'react-router-dom';
import {fetchCategories} from '../../redux/actions/category/categoryAction';

class ArticleList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.fetchArticles();
    this.props.fetchCategories();
  }

  //Convert Date
  convertDate = (created_date) => {
    let parameterDate = created_date;
    let year = parameterDate.substring(0, 4);
    let month = parameterDate.substring(5, 7);
    let day = parameterDate.substring(8, 10);
    let formalDate = year + "-" + month + "-" + day;
    return formalDate;
  };

  render() {
    return (
      <div>
        <Container fluid="sm">
          <Row>
            <Col xs={5}>
              <h4>{this.props.translate("ArticleList.MANAGEMENT")}</h4>
            </Col>
            <Col>
              <Row>
                <Col>
                  <Form inline>
                    <Form.Label
                      className="my-1 mr-2"
                      htmlFor="inlineFormCustomSelectPref"
                    >
                      category
                    </Form.Label>

                    <Form.Control
                      as="select"
                      className="my-1 mr-sm-2"
                      id="inlineFormCustomSelectPref"
                      custom>
                       <option value="0">All</option>
                          {this.props.category.map((data)=>{
                            return(
                              <option value={data.name}>{data.name}</option>
                            )
                          })}
                    </Form.Control>

                     <Form.Control  type="text" placeholder="Search" />
                    
                     <Button variant="outline-success"> {this.props.translate("ArticleList.SEARCH")} </Button>
                     <Link to="/Add">
                      <Button type="submit" className="my-1">
                      {this.props.translate("ArticleList.CREATE_DATE")}
                      </Button>
                     </Link>

                  </Form>
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
        <br/>
        {
          this.props.article.map((data,index)=>{
            return <div key={index}>
                <Container>
                  <Row>
                    <Col xs={4}> <img src={data.image} width="100%" alt={data.image}/></Col>
                    <Col xs={8}>
                        <h5>{data.title}</h5>
                        <p className="text-muted">{this.props.translate("ArticleList.CREATE_DATE")} : {this.convertDate(data.createdAt)}</p>
                        <p className="text-muted">{this.props.translate("ArticleList.CATEGORY")} : Hot News</p>
                        <p>{data.description}</p>
                        <Row>
                          <Col>

                          {/* <Link to="/View">
                          <Button variant="primary" onClick={()=>{this.props.viewArticle(data._id)}}>VIEW</Button>	&nbsp;
                          </Link> */}

                          <Link to={`/View/${data._id}`}>
                          <Button variant="primary"> {this.props.translate("ArticleList.VIEW")} </Button>	&nbsp;
                          </Link>
                          
                           {/* <Button variant="warning" onClick={()=>{this.props.updateArticle(data)}}>EDIT</Button>	&nbsp; */}
                           <Button variant="warning"> {this.props.translate("ArticleList.EDIT")} </Button>	&nbsp;
                           <Button variant="danger"  onClick={()=>{this.props.deleteArticle(data._id) }}> {this.props.translate("ArticleList.DELETE")} </Button>
                          </Col>
                        </Row>
                    </Col>
                  </Row>
                  <hr />
                </Container>
                <br/>
                
               
            </div>
          // );
        })}

        <p className="text-center">
          Yay! You have seen it all <span role="img">👍🏌️‍♂️🥃</span>
        </p>
      </div>
    );
  }
}



const mapStateToProps = (state) =>{
  return{
    article : state.articleReducer.article,
    articleDetail : state.articleReducer.articleDetail,
    category : state.categoryReducer.category
  }
}

const mapDispatchToProps = (dispatch)=>{
  const boundActionCreators = bindActionCreators({
    fetchArticles,
    deleteArticle,
    viewArticle,
    fetchCategories,
  },dispatch)
  return boundActionCreators
}

export default connect(mapStateToProps, mapDispatchToProps)(ArticleList);
